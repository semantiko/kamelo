open Common.Type
(* open Common.Xlib_OCaml *)
open Interface.Signature
open Terminal.Display_console

open Controller.Prelude (* TODO delete *)
open Common.Error

let create_file name =
  (* Add the correct extension (.dk or .lp) *)
  let filename = Terminal.Cmd_line.create_filename name in (* TODO *)
  (* Create the file *)
  let f  = open_out filename in
  let ff = Format.formatter_of_out_channel f in (f, ff)

let close_file f ff =
  Format.pp_print_flush ff ();
  close_out f ;
  flush stdout

(** [print_file ff path] prints via [ff] the content of the file located at [path]. *)
let print_file ff (path : string) =
  let flib  = open_in path in
  try
    while true; do
      print ff "%s\n" (input_line flib)
    done ;
  with End_of_file -> close_in flib

(** [print_root_import cd ff name] prints via [ff] an import declaration
    assuming the file named [name] is inside "sem_root".
    The counter [cd] is also updated. *)
    let print_root_import cd ff name =
      let path = ["sem_root"] in
      let i = (name, []) in
      let import_trans =
        TransSem.Import.import_to_require_open path i
      in
      Mecanism.Count_data.incr_real_import cd ;
      LP.LP_printer.pp_command ff import_trans

let () =
  (* STEP A: Pre-processing *)
  (*    STEP 1: Parsing the command-line    *)
  Terminal.Cmd_line.parse ();
  (*    STEP 2: Parsing the input Kore file *)
  let lexbuf = Lexing.from_channel (!Terminal.Cmd_line.input) in
  let file =
    try Parsing.Kparser.file Parsing.Klexer.token lexbuf
    with e -> red_msg_1 _STDOUT "Parsing fails line %i" !Parsing.Count_line.curr_line ; raise e
  in
  if !Terminal.Cmd_line.debug then
    (* let  =  in
    if !Terminal.Cmd_line.no_cleaning then
      List.map file
      Printing.Kore_printer.pp_kore_kommand ff cd kommand_l (* TODO *)
       else *)
    ()

  else
    (* c. Printing management *)
    let printing = match !Terminal.Cmd_line.output with
      | O_LP      -> LP.LP_printer.pp_command
      | O_Dedukti -> fun _ _ -> () (* @TODO *)
      (* | O_Kore    -> fun _ _ -> () (* Printer.pp_kore_kommand ff cd *) *)
    in
  (*

  (*    STEP 3: Printing management *)
  if !Terminal.Cmd_line.debug then

  else

    (** STEP B: Translate the input file *)
  if !Terminal.Cmd_line.semantics_file = "" (* if input file = semantics *)
  then
    (* STEP 1: Parse the K semantics file *)

    (* STEP 2: Pre-process the Kore file  *)

    (* STEP 3: Translate the Kore file    *)
    match !Terminal.Cmd_line.logic_used with
    | Interpreted ->

    | MSML ->

  else
    match !Terminal.Cmd_line.logic_used with
    | Interpreted -> (* if input file = program *)

    | MSML -> (* if input file = program specification *)


      *)



  (* STEP C: Translate the semantic or the executable *)
  match file with
  | F_pgm(exec, result) ->
    let cd = Mecanism.Count_data.reset_count_data 0 in
    (* STEP 1: Create the new file *)
    let f, ff = create_file !Terminal.Cmd_line.input_filename in
    (* STEP 2: Print the import of the semantic *)
    print_root_import cd ff (!Terminal.Cmd_line.semantics_file) ;

     (* STEP 3: Translate the executable *)
     let p_exec, _ (* free_var_data *) = TransPgm.Executable.iter_exec exec empty_sign in
     (* STEP 4: Print free variables *)
     (* let f_pp : string -> string list -> unit = fun key var_l ->
       let var_type =
         Interface.LP_p_term.create_appl
           Interface.K_prelude.p_INJD
           (Interface.LP_p_term.create_ident key) in
       let comm name =
         let _ = Controller.Prelude.pp_symbol_prelude ff cd
           (LP.LP_printer.pp_command) empty_sign
           (Interface.LP_p_term.create_symbol name var_type) in ()
       in
       List.iter (fun name -> comm name) var_l
     in
        StrMap.iter f_pp free_var_data ; TODO not useful, right? *)
     (* STEP 5: Translate the result of the executable *)
     let p_res, _ = TransPgm.Executable.iter_exec result empty_sign in
     (* STEP 6: Print the symbol s_e which represents the executable *)
     LP.LP_printer.pp_command ff
       (Interface.LP_p_term.create_LP_symbol
          (Interface.LP_p_term.create_symbol_with_body "PGM" p_exec)) ;
     (* STEP 7: Print the symbol s_r which represents the result *)
     LP.LP_printer.pp_command ff
       (Interface.LP_p_term.create_LP_symbol
          (Interface.LP_p_term.create_symbol_with_body "RES" p_res)) ;
     (* STEP 8: Print the assert command to check s_e == s_r *)
     let s_e = Interface.LP_p_term.create_ident "PGM" in
     let s_r = Interface.LP_p_term.create_ident "RES" in
     LP.LP_printer.pp_command ff
       (Interface.LP_p_term.create_assert_command s_e s_r) ;
     (* STEP 9: Close the new file *)
     close_file f ff
  | F_spec_pgm _ -> (* failwith "TODO" *) TransPgmSpec.Data_trace.get_KProver_proof_objects ()
  | F_sem (_, file) ->
     (* STEP 1: Pre-processing *)
        (* a. Split the file and get main informations *)
    let filename, kseq_module, semantics_module =
      match file with
      | [("BASIC-K",_,_,_);(("KSEQ",_,_,_) as m2);("INJ",_,_,_);("K",_,_,_);((x,_,_,_) as m5)] -> x, m2, m5
      | _ -> raise (KaMeLoError (InternalError, "Main", "semantics_module_name", "The prelude hasn't the expected shape."))
    in
        (* b. Create the new file *)
    let f, ff = create_file filename in





     (* STEP 2: The main function to translate one Kore module *)
     let module_to_file : signature -> kmodule -> signature =
       fun sign m ->
       (* let name, import_l, command_l, attribut_l = m in *)
       let name, _, kommand_l, _ = m in
          (* a. Cleaning the semantic files *)
       let kommand_l =
         if not(name = "BASIC-K" || name = "KSEQ" || name = "INJ" || name = "K") then
           Controller.Cleaning.cleaning kommand_l
         else kommand_l
       in
       Common.Error.print ff "\n// Translation of the module ";
       Format.pp_print_string ff name;
       Common.Error.print ff "\n";
          (* b. Reset count data *)
       let cd = Mecanism.Count_data.reset_count_data 0 in
          (* c. Translation management *)
       let sign_bis =
         if !Terminal.Cmd_line.old then
           (Controller.Old.first_translation ff cd m ; sign)
         else
           begin
             let res, new_sign =
               if !Printing.Meta_printer.lib
               then Controller.With_Viry_encoding.main_with_lib cd kommand_l sign
               else Controller.With_Viry_encoding.main_without_lib cd kommand_l sign
             in
             (match !Terminal.Cmd_line.mimic with
              | M_Kore    ->
                 (match !Terminal.Cmd_line.output with
                  | O_LP ->
                     Printing.Meta_printer.prt_Viry ff cd printing res
                  | O_Dedukti ->
                     Printing.Sem_printer.pp_sem_kommand ff cd kommand_l)
                  (*| O_Kore ->
                     Printing.Kore_printer.pp_kore_kommand ff cd kommand_l) *)
              | M_K       ->
                 Printing.Meta_printer.prt_Viry ff cd printing res
              | M_Dedukti -> ()) ; new_sign
           end
       in
          (* d. Printing count data *)
       print_module_message name (List.length kommand_l) cd;
       Format.pp_print_flush ff () ; sign_bis
     in

     (* STEP 3: Run the main translation *)
     print_header_kamelo () ;
     (if false
      then
        (print ff "%s" "symbol typeK : TYPE;\n" ;
         print ff "%s" "symbol iK : typeK → TYPE;\n" ;
         print ff "%s" "symbol #EQUALS R1 R2 : iK R1 → iK R1 → iK R2;\n" ;
         print ff "%s" "symbol #EXISTS RV R : (iK RV → iK R) → iK R;\n"  ;
         print ff "%s" "symbol #AND R : iK R → iK R → iK R;\n" ;
         print ff "%s" "symbol #OR R : iK R → iK R → iK R;\n" ;
         print ff "%s" "symbol #NOT R : iK R → iK R;\n" ;
         print ff "%s" "symbol #IMPLIES R : iK R → iK R → iK R;\n" ;
         print ff "%s" "symbol #BOTTOM R : iK R;\n" ;
         print ff "%s" "symbol #TOP R : iK R;\n" ;
         print ff "%s" "symbol #REWRITES R : iK R → iK R → iK R;\n" ;
         print ff "%s" "symbol #IN R1 R2 : iK R1 → iK R2 → iK R2;\n" ;
         print ff "%s" "symbol #DV R : iK R → iK R;\n" ;
         print ff "%s" "symbol #CEIL R1 R2 : iK R1 → iK R2;\n" ;
         print ff "%s" "symbol d : Π (x : typeK), iK x → TYPE;\n" ;
         print ff "%s" "symbol SortBool : typeK;\n" ;
         print ff "%s" "symbol true : iK SortBool;\n" ;
         print ff "%s" "symbol false : iK SortBool;\n" ;
         print ff "%s" "symbol SortInt : typeK;\n" ;
         print ff "%s" "symbol 0 : iK SortInt;\n" ;
         print ff "%s" "symbol 1 : iK SortInt;\n" ;
        )) ;
     if !Printing.Meta_printer.lib
     then
       ((* a. The K standard library *)



         print_file ff "src/K_Builtin/interpreted/lib.lp" ;
         (* b. Translation of KSEQ and K modules, and the semanctics *)
         let _ = List.fold_left module_to_file empty_sign [kseq_module;semantics_module] in () )
     else
       ((* a. Translation of BASIC-K module *)
         let sign_g = module_to_file empty_sign (List.hd file) in
        (* b. Printing of the K prelude interface *)
         print_comment ff "PRELUDE";
         let sign_res =
           Controller.Prelude.create_prelude ff printing sign_g "prelude"
         in
        (* c. Translation of KSEQ, INJ, K modules, and the semantics *)
         let _ = List.fold_left module_to_file sign_res (List.tl file) in () ) ;

     print_footer_kamelo ();

     (* STEP 4: Close the new file *)
     close_out f ;
     flush stdout;;
