The KaMeLo source file hierarchy  
================================  
 
   src/  
   ├── main.ml: *The main algorithm to translate a semantics or an executable*  
   ├── K_builtin/interpreted/lib.lp: *A partial manual translation of the K standard library*  
   ├── common/  
   │   ├── color.ml: *Some colors for printing in the terminal*  
   │   ├── error.ml: *Some error messages and execeptions*  
   │   ├── getter.ml: *Some functions on abstract Kore file*  
   │   ├── type.ml: *Type to abstract Kore file*  
   │   └── xlib_OCaml.ml: *Extension of the OCaml standard library*  
   ├── controller/  
   │   ├── cleaning: *To clean before translating*  
   │   ├── old.ml: *The first translation (use the option --old)*  
   │   ├── prelude.ml: *To print the K prelude interface*  
   │   └── with_Viry_encoding.ml: *A translation with Viry encoding*  
   ├── interface/  
   │   ├── getter_term.ml: *Some getters on K and Dedukti*  
   │   ├── K_prelude.ml: *Some specific term of K*  
   │   ├── LP_p_term.ml: *Interface with Lambdapi or Dedukti*  
   │   ├── output.ml: **This file need to move in printing/**  
   │   └── signature.ml: *Data-structure to collect data during the translation*  
   ├── LP/: **This folder MUST BE DELETED**  
   │   ├── LP_printer.ml  
   │   ├── pos.ml  
   │   └── syntax.ml  
   ├── mecanism/: *The main structure of the translation*  
   │   ├── axiom_iterator.ml: *To iterate over an axiom*  
   │   ├── count_data.ml: *Data structure to recap the translation in the terminal*  
   │   └── kommand_iterator.ml: *To iterate over Kore commands*  
   ├── parsing/: *To parse Kore file*  
   │   ├── count_line: *To find out which command fails (useful for debugging)*  
   │   ├── klexer.mll: *The Kore lexer*  
   │   └── kparser.mly: *The Kore parser*  
   ├── printing/  
   │   ├── Kore_printer.ml: *To print into a simplified Kore syntax*  
   │   ├── meta_printer.ml: *Meta-printers to print the resulting translation*  
   │   └── rewrite.py: *Run this script to rewrite a executable program*  
   ├── terminal/: *Management of the terminal*  
   │   ├── cmd_line.ml: *To parse the command line*  
   │   └── display_console.ml: *To print the recap of the translation in the terminal*  
   └── translating/: *The translation of...*  
        ├── axiom.ml:                      *... the axioms*  
        ├── eval_strategy.ml:         *... the evaluation strategies*  
        ├── executable.ml:             *... the executables*  
        ├── import.ml:                     *... file importation*  
        ├── prelude_interface.ml: *... the K prelude interface*  
        ├── sort.ml:                         *... the sorts*  
        ├── symbol.ml:                   *... the symbols*  
        └── viry.ml:                         *... the conditional rewriting rules*  

Note: The files "dune" are just here to compile the OCaml code.  
