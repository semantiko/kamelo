val no_color : bool ref

val red : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
          ('a, 'b, 'c, 'd, 'e, 'f) format6
val gre : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
          ('a, 'b, 'c, 'd, 'e, 'f) format6
val yel : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
          ('a, 'b, 'c, 'd, 'e, 'f) format6
val blu : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
          ('a, 'b, 'c, 'd, 'e, 'f) format6
val mag : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
          ('a, 'b, 'c, 'd, 'e, 'f) format6
val cya : ('a, 'b, 'c, 'd, 'e, 'f) format6 ->
          ('a, 'b, 'c, 'd, 'e, 'f) format6
